﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace section01.Models
{
    public class Band
    {
        [Display(Name="Name og the musican")]
        public string Title { get; set; }
        [Display(Name = "Name og the gendre")]
        public string Gendre { get; set; }
        [Display(Name = "Name og the origin")]
        public string Origin { get; set; }
        
        public string Biography { get; set; }
    }
}
