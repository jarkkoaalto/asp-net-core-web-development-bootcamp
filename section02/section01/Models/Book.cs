﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace section01.Models
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }
}
