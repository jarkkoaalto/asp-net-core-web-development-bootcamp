﻿using section01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace section01.Repository
{
    interface IBandRepository
    {
        /// <summary>
        /// Get all bands in the database
        /// </summary>
        /// <returns>A List of bands</returns>
        List<Band> GetBands();

        Band GetBand(int id);
    }
}
