﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using section01.Models;

namespace section01.Repository
{
    public class BandRepository : IBandRepository
    {
        public BandRepository(Mock<BandRepository> mockRepository)
        {
        }

        public Band GetBand(int id)
        {
            throw new NotImplementedException();
        }

        public List<Band> GetBands()
        {
            var listOfBands = new List<Band>()
            {
                new Band
                {
                    Title = "Test and again",
                    Biography = "Should work",
                    Origin = "Parents Garage",
                    Gendre = "Rock"
                },
                 new Band
                {
                    Title = "Caliban",
                    Biography = "Let go",
                    Origin = "Parents Garage",
                    Gendre = "HeavyRock"
                },
                  new Band
                {
                    Title = "Korn",
                    Biography = "Only field",
                    Origin = "Parents Barn",
                    Gendre = "Altenative"
                }
            };
            return listOfBands;
        }
    }
}
