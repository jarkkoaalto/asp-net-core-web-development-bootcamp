﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using section01.Models;
using section01.Repository;

namespace section01.Controllers
{
    public class MusicController : Controller
    {
        private readonly IBandRepository _bandRepository;
        private IBandRepository repository;

        public MusicController(BandRepository @object)
        {
            _bandRepository = repository;
        }

        // GET: Music
        public ActionResult Index()
        {
           
            return View(_bandRepository.GetBands());
        }

        // GET: Music/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public JsonResult TestGetBand()
        {
            var band = new Band
            {
                Title = "Newiest band",
                Biography = "Should woks better than earlier",
                Origin = "Your father grage",
                Gendre = "Garage rock"
            };
            return Json(JsonConvert.SerializeObject(band));
        }

        // GET: Music/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Music/Create
        [HttpPost]
       // [ValidateAntiForgeryToken]
        public ActionResult Create([FromForm]Band band)
        {
            try
            {

                // TODO: Add insert logic here
                // return RedirectToAction(nameof(Index));
                if (ModelState.IsValid)
                {
                    Console.WriteLine("Valid mo fo");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Music/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Music/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Music/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Music/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}