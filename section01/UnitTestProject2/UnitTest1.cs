using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using section01.Controllers;
using section01.Models;
using section01.Repository;
using System.Collections.Generic;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ControllerDoesNotReturnNull()
        {
            var mockRepository = new Mock<BandRepository>();
            mockRepository.Setup(x => x.GetBands()).Returns(new List<Band>
            {
                new Band
                {
                    Title = "This is mock",

                }

            });
           
            var controller = new MusicController(mockRepository.Object);
            Assert.IsNotNull(controller);
        }
    }
}
